# rushi-af-motor-shield
Tested OK code written for RUSHI-AF-MOTOR-SHIELD
/*
 **************************************************************************************************
 * Title  : README file 
 * Board  : RUSHI-AF-MOTOR-SHIELD
 * Sketch : Controls a 2WD/4WD robot over a bluetooth link
 * Author : Written by Rushikesh Deshmukh https://sites.google.com/view/rushikeshdeshmukh
 * License: Berkeley Software Distribution (BSD)
 **************************************************************************************************
 */